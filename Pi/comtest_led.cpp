#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <unistd.h> //UNIX standard function definitions
#include <fcntl.h> //File control definitions
#include <errno.h> //Error number defial control definitions
#include <sys/ioctl.h>nitions
#include <termios.h> //POSIX termin
#include <ctime>

#define COMMAND_START 0xCA
#define COMMAND_END 0xAC

#define COMMAND_LENGTH_NOLIGHT 0
#define COMMAND_LENGTH_SOLID 0


#define COMMAND_TIMEOUT 10

typedef enum {
  COMMAND_NOLIGHT = 0,
  COMMAND_SOLID,
  COMMAND_GET_CURRENT_TEMP,
  COMMAND_RESET,
  COMMAND_RESET_CLEAR_DATA,
  COMMAND_MAX
} Command;

typedef enum {
  RESPONSE_OK = 0,
  RESPONSE_ERROR,
  RESPONSE_MAX
} ResponseType;

typedef enum {
  ERROR_GENERIC = 0,
  ERROR_BAD_COMMAND,
  ERROR_BAD_DATA_LENGTH,
  ERROR_BAD_DATA_CONTENT,
  ERROR_MAX
} ErrorType;

/*
 * Opens the specified serial port
 * Returns file descriptor, or -1 on error
 */
int open_port(std::string port){

    //Ugly hack to make serial port behave.
    //Remove once settings needed to open properly are discovered.
    char* sttyCommand = new char[1000];
    sprintf(sttyCommand, "stty -F %s 115200 raw", port.c_str());
    std::cout << "Stty command: " << sttyCommand << std::endl;
    system(sttyCommand);
    //Need to wait for command to run.
    sleep(1);
    delete sttyCommand;


    int fd; //File descriptor for port

    //RDWR specifies read/write
    //NOCTTY makes this program not respond to any possible keyboard input commands sent back from the arduino
    //NDELAY will prevent block if serial voltage is not connected.
    fd = open(port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

    //Check for error
    if(fd == -1){
        //Failed to open port
        std::cout << "Unable to open port. Error: " << port << std::endl;
        perror("");
    } else {
        //Set file control flags for the serial port file descriptor.
        fcntl(fd, F_SETFL, 0); //??

        //Set properties on the serial port
        struct termios options;

        //Gets the current terminal control struct for the port
        tcgetattr(fd, &options);

        //Set baud rate to 115200
        cfsetispeed(&options, B115200);
        cfsetospeed(&options, B115200);

        //Force raw input. Needed for Raspberry Pi?
        options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

        //Apply settings immediately
        tcsetattr(fd, TCSANOW, &options);
    }

    return fd;
}

/*
 * Writes data to the serial port
 * Takes file descriptor, data, and data length in bytes
 */
void write_data(int port_fd, char* data, int len){
    int written = write(port_fd, data, len);
    std::cout << "Data written: " << written << std::endl;
    if(written < len){
        std::cout << "Failed to write data to serial port!" << std::endl;
    }
}

int read_data(int port_fd, char* data, int maxlen){
    int bytesRead = read(port_fd, data, maxlen);
    if(bytesRead < 0){
        std::cout << "Unable to read data from port!" << std::endl;
    }
    return bytesRead;
}


bool prompt(){
    bool exit = false;
    return exit;
}

int main(int argc, char** argv){
    //Whether or not to ignore responses from the arduino
    bool noResponse = false;
    //Exit flag for the main loop
    bool exit = false;


    std::cout << "Serial Communication Test" << std::endl;

    //Open the serial port
    int fd = open_port(argv[1]);

    //Check if we should ignore any responses back from the arduino
    if((argc > 2) && !(strcmp(argv[2], "-nr"))){
        noResponse = true;
    }

    std::cout << "File descriptor for port: " << +fd << std::endl;

    //Only allow command input if we have a valid serial port file descriptor
    if(fd >= 0){

        while(!exit){
            std::cout << "Enter a choice: " << std::endl;
            std::cout << "1) LED Off" << std::endl;
            std::cout << "2) LED On" << std::endl;
            std::cout << "3) Get Temperature" << std::endl;
            std::cout << "4) Reset Arduino" << std::endl;
            std::cout << "5) Reset Arduino and Clear Save Data" << std::endl;
            std::cout << "6) Exit" << std::endl;
            std::cout << ">";

            char buffer[256];
            memset(buffer, 0, 256);
            char* asciiConversionBuffer;

            int commandLength = 0;
            bool writeCommand = true;
            int responseLength = 0;
            int expectedResponseLength = 2;

            float temperatureInput = 0;
            unsigned int blinkTime;

            int input;
            std::cin >> input;

            buffer[0] = COMMAND_START;

            switch(input){
                case 1:
                    buffer[1] = COMMAND_NOLIGHT;
                    buffer[2] = 0;
                    buffer[3] = COMMAND_END;
                    commandLength = 4;
                    break;
                case 2:
                    buffer[1] = COMMAND_SOLID;
                    buffer[2] = 0;
                    buffer[3] = COMMAND_END;
                    commandLength = 4;
                    break;
                case 3:
                    buffer[1] = COMMAND_GET_CURRENT_TEMP;
                    buffer[2] = 0;
                    buffer[3] = COMMAND_END;
                    commandLength = 4;
                    expectedResponseLength = 7;
                    break;
                case 4:
                    buffer[1] = COMMAND_RESET;
                    buffer[2] = 0;
                    buffer[3] = COMMAND_END;
                    commandLength = 4;
                    exit = true;
                    break;
                case 5:
                    buffer[1] = COMMAND_RESET_CLEAR_DATA;
                    buffer[2] = 0;
                    buffer[3] = COMMAND_END;
                    exit = true;
                    commandLength = 4;
                    break;
                case 6:
                    writeCommand = false;
                    exit = true;
                    break;
            }

            Command enteredCommand = (Command)buffer[1];

            if(writeCommand){
                //Print packets to console so if there are problems, we can more easily debug.
                std::cout << "Packet to send (Bytes): ";
                for(int packetIndex = 0; packetIndex < commandLength; packetIndex++){
                    std::cout << +buffer[packetIndex] << " ";
                }
                std::cout << std::endl;

                std::cout << "Packet to send (ASCII): ";
                for(int packetIndex = 0; packetIndex < commandLength; packetIndex++){
                    std::cout << buffer[packetIndex] << " ";
                }

                std::cout << std::endl;

                std::cout << "Sending command..." << std::endl;
                write_data(fd, buffer, commandLength);

                if(noResponse){
                    std::cout << "Debugging measure: Continue. Look at Arduino serial monitor for command result" << std::endl;
                    continue;
                }

                std::cout << "Awaiting response of length " << expectedResponseLength << "..." << std::endl;

                //Get the current time so we can determine if we've timed out.
                std::time_t startTime = std::time(nullptr);

                //Loop until we have all of the expected data or we time out.
                responseLength = 0;
                bool timeout = false;
                while(responseLength < expectedResponseLength){
                    //Get the number of bytes available to read on the serial port.
                    ioctl(fd, FIONREAD, &responseLength);

                    //Check if we timed out
                    if(std::time(nullptr) - startTime > COMMAND_TIMEOUT){
                        std::cout << "Timed out waiting for complete response! (" << responseLength << " of " << expectedResponseLength << ")" << std::endl;
                        timeout = true;
                        break;
                    }
                }

                //If we have timed out, restart the loop.
                if (timeout) continue;

                std::cout << "Reading response..." << std::endl;
                read_data(fd, buffer, 256);

                if(buffer[0] == RESPONSE_OK){
                    std::cout << "Command successfully sent and parsed (" << +buffer[0] << ", " << +buffer[1] << ")" << std::endl;

                    if(buffer[1] != enteredCommand){
                        std::cout << "Warning - command in returned result is different from entered command " << +enteredCommand << std::endl;
                    }

                    std::cout << "Response length: " << +buffer[2] << std::endl;;

                    //Certain commands may send back specific data.
                    switch(enteredCommand){
                        case COMMAND_GET_CURRENT_TEMP:
                            //Get temperature by converting from ASCII string
                            //Since there is no null terminator, need to use the second argument to specify the final character.
                            asciiConversionBuffer = new char[buffer[2]];
                            memcpy(asciiConversionBuffer, buffer + 3, buffer[2]);

                            temperatureInput = atof(asciiConversionBuffer);
                            std::cout << "Debug - raw response: " << buffer + 2 << std::endl;
                            delete asciiConversionBuffer;
                            asciiConversionBuffer = nullptr;

                            std::cout << "Temperature: " << temperatureInput << " F" << std::endl;
                            break;
                        default:
                            std::cout << "No extra data for response to command " << +buffer[1] << std::endl;
                            if (buffer[2] > 0){
                                std::cout << "Unexpected data present in response. Packet:" << std::endl;
                                for(int packetIndex = 0; packetIndex < buffer[2]; packetIndex++){
                                    std::cout << +buffer[packetIndex] << " ";
                                }
                                std::cout << std::endl;
                            }
                            break;

                    }

                } else {
                    std::cout << "Something went wrong with this command: Response " << +buffer[0] << " command " << +buffer[1] << " error code " << +buffer[2] << std::endl;
                }
            }
        }
    }

    close(fd); //Close the file descriptor before exit
    return 0;
}


