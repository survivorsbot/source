
#include <EEPROM.h> // Used to save configuration information while turned off.
//#include <Wire.h> // Used to establied serial communication on the I2C bus
#include "DHT.h" //Used to send and recieve specific information from temperature sensor
//#include "Adafruit_Sensor.h"
#include "constants.h"

//Prototypes
Command getCurrentCommand();
void loadSettings();
void saveSettings();
void ledOn();
void ledOff();
float getTemp();
int getMotionStatus();
void resetArduino();
void parseCommand();
void resetCommandReceive();


//String inputString = "";         // a string to hold incoming data

bool isReceivingCommand = false;
int commandRecieveStartTime = 0;

byte currentCommandPacket[255];
int currentCommandIndex = 0;

boolean stringComplete = false;  // whether the string is complete


void (*ledCommand)() = NULL;
void (*fanCommand)() = NULL;

//Initialize sensor at I2C address 0x48
//TMP102 sensor0(0x48);

//Initialize sensor
DHT dht(DHTPIN, DHTTYPE);


void setCurrentLEDCommand(Command command){
  if(command == COMMAND_SOLID){
    ledCommand = ledOn;
  } else if (command == COMMAND_NOLIGHT){
    ledCommand = ledOff;
}
}

void loadSettings(){
  int address = SAVE_ADDRESS_BASE;
  
  //Load order must match save order.
  
  //Only preceed if the first value is set.
  if(EEPROM.read(address) == SAVE_START_VALUE){
    address++;
    

  }
}

void saveSettings(){
  int address = SAVE_ADDRESS_BASE;
  
  //Save order must match load order.
  
  EEPROM.write(address, SAVE_START_VALUE);
  address++;
}

void ledOn(){
  //Turn and leave LED on
  digitalWrite(PIN_LED, HIGH);
}

void ledOff(){
  //Turn and leave LED off.
  digitalWrite(PIN_LED, LOW);
}

float getTemp(){

  float toReturn = 0.0;

  //delay(2000);

  toReturn = dht.readTemperature(true);
  
  return toReturn;
}

int getMotionStatus(){
    volatile int toReturn;
    toReturn = digitalRead(MOTION_SENSOR);

    return toReturn;
}

/*void resetArduino(){
  //There is no API to reset the arduino, so it must be done from assembly.
  //Set CPU program counter to address 0. Arduino flash, and therefore the program, start here.
  asm volatile ("jmp 0");
}*/

/* Calls the appropriate function for the current given command.
 *
 * FORMAT:
 * -1 byte command code
 * -1 byte command length (following command code and length bytes)
 * -n bytes command data
 */
void parseCommand(){
  Command command = static_cast<Command>(currentCommandPacket[0]);
  int commandLength = (int)currentCommandPacket[1];
  bool commandParsed = true;
  ErrorType error = ERROR_GENERIC;
  char responseData[255];
  byte responseLength = 0;
  
  switch(command){
    case COMMAND_NOLIGHT:
      ledCommand = ledOff;
      break;
    case COMMAND_SOLID:
      ledCommand = ledOn;
      break;
    case COMMAND_GET_CURRENT_TEMP:
      //Arduino does not support sprintf with %f format, so use dtostrf instead.
      dtostrf(getTemp(), 4, 1, responseData);
      responseData[6] = '\0';
      //Four characters, one decimal precision, plus decimal itself and null terminator.
      responseLength = 7;
      break;
    case COMMAND_GET_MOTION_STATUS:
      dtostrf(getMotionStatus(),4,1, responseData);
      responseData[6] = '\0';
      responseLength = 7;
      break;
    case COMMAND_RESET_CLEAR_DATA:
      //"Clear" data by clearing the initialized flag and setting variables back to default values.
      EEPROM.write(SAVE_ADDRESS_INITIALIZED, 0);
    case COMMAND_RESET:
      //Calling reset restarts the program, so execution will not continue from here!
      //resetArduino();
      break;
     default:
      commandParsed = false;
      error = ERROR_BAD_COMMAND;
   }
  
  if(commandParsed){
    Serial.write(RESPONSE_OK);
    Serial.write((byte)command);
    
    if(responseLength > 0){
      Serial.write(responseLength);
      for(int responseByteIndex = 0; responseByteIndex < responseLength; responseByteIndex++){
        Serial.write(responseData[responseByteIndex]);
      }
    }
    
    //Save current state so we can restore if losing power
    saveSettings();
  } else {
    Serial.write(RESPONSE_ERROR);
    Serial.write(currentCommandPacket[0]);
    Serial.write(error);
  }
}

void setup() {
  // initialize pins
  pinMode(PIN_LED, OUTPUT);
  //pinMode(PIN_TEMP_ALERT, INPUT);
  pinMode(MOTION_SENSOR, INPUT);
  

 //Starting the temp sensor
//  dht.begin();
  Serial.begin(115200);

  
  //Load any previously saved settings if they exist
  loadSettings();
}

void loop() {
  //blinkLED();
  
  //Run the current led command if set
  if(ledCommand != NULL){
    ledCommand();
  }  
  
  //Run the current fan mode if set
  if(fanCommand != NULL){
    fanCommand();
  }
  
  //Check for events
  //Need to poll because serialEvents interrupt is not supported on Micro.
  pollSerialEvents();
  
  //Check if we have timed out on receiving a command
  if(isReceivingCommand && (millis() - commandRecieveStartTime >= TIMEOUT_RECIEVE_COMMAND)){
    resetCommandReceive();
  }
}

void pollSerialEvents() {
  while (Serial.available()) {
    // get the new byte:
    byte inChar = static_cast<byte>(Serial.read());
    
    if(isReceivingCommand){
      currentCommandPacket[currentCommandIndex] = inChar;
      currentCommandIndex++;
            
      if(currentCommandIndex > 1){
        byte packetLength = currentCommandPacket[1];
        
        //If we hit the end of the packet, make sure we are actually done.
        //+3 to skip the command and command length bytes
        if(currentCommandIndex == (packetLength + 3)){
          //If the current character is the end command character, packet transfered successfully.
          if(inChar == COMMAND_END){
            parseCommand();
          } else {
            //Packet did not transfer successfully.
            Serial.write(RESPONSE_ERROR);
            Serial.write(currentCommandPacket[0]);
            Serial.write(ERROR_BAD_DATA_LENGTH);
          }
          
          //Whether command succeeded or not, we are done with this packet.
          resetCommandReceive();
        }
      }
    } else {
      //If we have recieved the start command, set the receiving data flag.
      if(inChar == COMMAND_START){
        //Serial.write("\nCommand start found");
        isReceivingCommand = true;
        commandRecieveStartTime = millis();
      } 
    }
  }
}

void resetCommandReceive(){
  isReceivingCommand = false;
  currentCommandIndex = 0;
  commandRecieveStartTime = 0;
}


