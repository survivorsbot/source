#ifndef CONSTANTS
  #define CONSTANTS
#endif

#define DHTPIN 7
#define DHTTYPE DHT22 //DHT 22 (AM2303)
#define MOTION_SENSOR 5
#define PIN_LED 6

//Identifiers for the start and end of a packet
#define COMMAND_START 0xCA
#define COMMAND_END 0xAC

//Identifier for the start of saved settings data. Used to tell if saved settings are present in EEPROM
#define SAVE_START_VALUE 0xAA

//Addresses for save data. Used so that if we have to relocate, we can do so easily.
#define SAVE_ADDRESS_BASE 0
#define SAVE_ADDRESS_INITIALIZED SAVE_ADDRESS_BASE
#define SAVE_ADDRESS_CURRENT_LED_COMMAND 1
//Keep in mind that blink time will take two bytes.
#define SAVE_ADDRESS_BLINK_TIME 2

//Timeout for receiving a command.
#define TIMEOUT_RECIEVE_COMMAND 100000

//Possible command responses to send.
typedef enum {
  RESPONSE_OK = 0,
  RESPONSE_ERROR,
  RESPONSE_MAX
} ResponseType;

//Possible error types to include when the response type is error.
typedef enum {
  ERROR_GENERIC = 0,
  ERROR_BAD_COMMAND,
  ERROR_BAD_DATA_LENGTH,
  ERROR_BAD_DATA_CONTENT,
  ERROR_MAX
} ErrorType;

//Commands that can be received.
typedef enum {
  COMMAND_NOLIGHT = 0,
  COMMAND_SOLID,
  COMMAND_GET_CURRENT_TEMP,
  COMMAND_GET_MOTION_STATUS,
  COMMAND_RESET,
  COMMAND_RESET_CLEAR_DATA,
  COMMAND_MAX
} Command;

